import { Box, Button, Flex } from '@chakra-ui/react';
import NavLink from 'next/link';
import React from 'react';
import { useLogoutMutation, useMeQuery } from '../generated/graphql';
import { isServer } from '../utils/isServer';

interface NavbarProps {}

const Navbar: React.FC<NavbarProps> = ({}) => {
  const [{ fetching: logoutFetching }, logout] = useLogoutMutation();
  const [{ data, fetching }] = useMeQuery({
    pause: isServer(),
  });

  let body = null;

  // LOADING
  if (fetching) {
    // USER BELUM LOGIN
  } else if (!data?.me) {
    body = (
      <>
        <NavLink href="/login">
          <Button
            colorScheme="teal"
            variant="solid"
            height="38px"
            width="100px"
            mr="8"
          >
            Login
          </Button>
        </NavLink>
        <NavLink href="/register">
          <Button
            colorScheme="purple"
            variant="solid"
            height="38px"
            width="100px"
          >
            Register
          </Button>
        </NavLink>
      </>
    );
    // USER SUDAH LOGIN
  } else {
    body = (
      <Flex>
        <Box mr={4}>{data.me.username}</Box>
        <NavLink href="/">
          <Button
            variant="link"
            isLoading={logoutFetching}
            onClick={() => {
              logout();
            }}
          >
            Logout
          </Button>
        </NavLink>{' '}
      </Flex>
    );
  }

  return (
    <Flex bg="facebook" p="7">
      <Box ml="auto">{body}</Box>
    </Flex>
  );
};
export default Navbar;
