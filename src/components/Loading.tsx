import React from 'react';
import Typewriter from 'typewriter-effect';

interface LoadingProps {}

const Loading: React.FC<LoadingProps> = ({}) => {
  return (
    <Typewriter
      onInit={setUp => {
        setUp.typeString('Loading...').start();
      }}
    />
  );
};
export default Loading;
