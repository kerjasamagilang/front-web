import { withUrqlClient } from 'next-urql';
import { createUrqlClient } from '../utils/createUrqlClient';
import Navbar from '../components/Navbar';
import { usePostsQuery } from '../generated/graphql';
import Loading  from "../components/Loading";

interface indexProps {}

const Index: React.FC<indexProps> = ({}) => {
  const [{ data }] = usePostsQuery();
  return (
    <div>
      <Navbar />
      <div>Hello Gilang</div>
      {!data ? <Loading /> : data.posts.map(p => <div key={p.id}>{p.title}</div>)}
    </div>
  );
};
export default withUrqlClient(createUrqlClient, {ssr: true})(Index);
