import { Heading, Box, Button } from '@chakra-ui/react';
import { Formik, Form } from 'formik';
import { NextPage } from 'next';
import { withUrqlClient } from 'next-urql';
import { useRouter } from 'next/dist/client/router';
import React from 'react';
import { useState } from 'react';
import InputField from '../../components/InputField';
import Wrapper from '../../components/Wrapper';
import { useGantiPasswordMutation } from '../../generated/graphql';
import { createUrqlClient } from '../../utils/createUrqlClient';
import { toErrorMap } from '../../utils/toErrorMap';

const LupaPassword: NextPage<{ token: string }> = ({ token }) => {
  const router = useRouter();
  const [tokenError, setTokenError] = useState('');
  const [, lupaPassword] = useGantiPasswordMutation();
  return (
    <Wrapper variant="small">
      <Heading mb={5}>Ganti Password Baru</Heading>
      <Formik
        initialValues={{ passwordBaru: '' }}
        onSubmit={async (values, { setErrors }) => {
          const response = await lupaPassword({
            passwordBaru: values.passwordBaru,
            token,
          });
          if (response.data?.lupaPassword.errors) {
            const errorMap = toErrorMap(response.data.lupaPassword.errors);
            if ('token' in errorMap) {
              setTokenError(errorMap.token);
            }
            setErrors(errorMap);
          } else if (response.data?.lupaPassword.user) {
            router.push('/');
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Box mt={5}>
              <InputField
                name="passwordBaru"
                placeholder="masukan password baru"
                label="Password Baru"
                type="password"
              />
            </Box>
            {tokenError ? <Box color="red">{tokenError}</Box> : null}
            <Button
              width="200px"
              height="38px"
              mt={4}
              type="submit"
              isLoading={isSubmitting}
              colorScheme="teal"
            >
              Ganti Password
            </Button>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
};

LupaPassword.getInitialProps = ({ query }) => {
  return {
    token: query.token as string,
  };
};

export default withUrqlClient(createUrqlClient, {ssr: false})(LupaPassword);
